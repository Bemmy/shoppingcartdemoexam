/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bmakonne
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddProductRegular() {
        System.out.println("addProduct Regualr Scenario");
        Product product = new Product("Blouse", 45.99);
        Cart instance = new Cart();
        instance.addProduct(product);
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testAddProductException(){
        
        System.out.println("addProduct Exception Scenario");
        Product product = null;
        Cart instance = new Cart();
        instance.addProduct(product);
        //fail("The test case is a prototype.");
        
    }

}
