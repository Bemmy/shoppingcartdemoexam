/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

public class ShoppingCartDemo {

    public static void main(String[] args) {
        
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentService creditService = factory.getPaymentService(PaymentServiceType.CREDIT);
        PaymentService debitService = factory.getPaymentService(PaymentServiceType.DEBIT);
        
        DiscountFactory discountFactory = DiscountFactory.getInstance();
        
        Cart cart = new Cart();
        cart.addProduct(new Product("shirt", 50));
        cart.addProduct(new Product("pants", 60));
        cart.setPaymentService(creditService);
        Discount  discount1 =  discountFactory.getDiscount(DiscountTypes.DOLLAR,10, cart.getCartTotal());
        cart.setDiscount(discount1);
        discount1.calculateDiscount(cart.getCartTotal());
        cart.payCart();
        
        cart.setPaymentService(debitService);
        Discount  discount2 =  discountFactory.getDiscount(DiscountTypes.DOLLAR,20, cart.getCartTotal());
        cart.setDiscount(discount2);
        discount2.calculateDiscount(cart.getCartTotal());
        cart.payCart();
        
    }
    
}
