/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author bmakonne
 */
public class DiscountFactory {
    
    private static DiscountFactory discountFactory;
    
    private DiscountFactory(){}
    
    public static DiscountFactory getInstance(){
        
        if(discountFactory == null) {
            
            discountFactory = new DiscountFactory();
        }
        
        return discountFactory;
    }
    
    public Discount getDiscount(DiscountTypes type, double discountAmount, double amount){
        
        Discount discount = null;
        
        switch(type){
            
            case DOLLAR : 
                discount = new DiscountByAmount(discountAmount, amount);
                break;
                
            case PERCENTAGE :
                discount = new DiscountByPercentage(discountAmount, amount);
                break;
        }
        
        return discount;
    }
    
}
