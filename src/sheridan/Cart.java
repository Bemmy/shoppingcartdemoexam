/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    
    private List<Product> products;
    private PaymentService service;
    private int cartSize;
    private Discount discount;
    
    public Cart(){
      
        products = new ArrayList<Product>();
    }
    
    public List<Product> getProducts(){
        return products;
    }
    
    public PaymentService getService(){
        return service;
    }
    public void setPaymentService(PaymentService service){
        this.service = service;
    }
    
    public void addProduct(Product product){
        products.add(product);
    }
    
    public double getCartTotal(){
        
        double totalPrice =0;

        for(Product p : products){
            totalPrice += p.getPrice();
        }
        
        return totalPrice;
    }
    
    public void payCart(){
        
        double totalPrice =0;
        double discountTotal = 0;
        
        for(Product p: products){
            totalPrice += p.getPrice();
        }
        
        
        if(discount.getDiscount() > 0){
            
        discountTotal = discount.calculateDiscount(totalPrice);
        
        totalPrice -= discountTotal;
        
        }
                service.processPayment(totalPrice);
    }
    
    public Discount getDiscountItem(){
        return discount;
    }
    
    public void setDiscount(Discount discount){
        this.discount = discount;
    }
    
    public int getCartSize(){
        
        cartSize = products.size();
        
        return cartSize;
    }
}
    

