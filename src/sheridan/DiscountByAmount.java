/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author bmakonne
 */
public class DiscountByAmount extends Discount {
    
    public DiscountByAmount(double discountAmount, double amount){
        this.discount = discountAmount;
        this.amount = amount;
    }
    
    @Override
    public double calculateDiscount(double amount){
        
        discount = amount - discount;
        
        return discount;
    }
    
}
